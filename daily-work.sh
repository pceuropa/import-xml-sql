#!/usr/bin/env bash

SCRIPT_DIR="/home/m/Desktop/localhost/import-xml-sql/download"
PY_FILE='/home/m/Desktop/localhost/import-xml-sql/main.py'
LOG_FILE='/home/m/Desktop/localhost/import-xml-sql/logs/cron.log'

date >> $LOG_FILE 2>&1
echo $USER >> $LOG_FILE 2>&1

wget http://torebkihurt.pl/xml-porownywarki/hurtownia/index.php?hash=dropshipping-y1 -O $SCRIPT_DIR/products.xml -nv -a $LOG_FILE
/home/m/Desktop/localhost/import-xml-sql/venv/bin/python3.7 $PY_FILE >> $LOG_FILE 2>&1
