#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from config_parser import settings
import xml.etree.ElementTree as ET
from helpers import XmltoList

"""
File: import_xml.py
Author: Rafal Marguzewicz
Email: info@pceuropa.net
Github: https://gitlab.com/pceuropa
Description:  Import products from xml
"""


class Import_from_XML(object):
    """Docstring for InterfaceFactory. """
    images = []
    attributes = []
    categories_mapping = settings['categories_mapping']
    producers = []

    def __init__(self):
        folder_xml = settings['folder_xml']
        filename_xml = settings['file_xml']
        self.path_file_xml = f'{folder_xml}/{filename_xml}'
        self.tree = ET.parse(self.path_file_xml)
        self.root = self.tree.getroot()[1]
        self.products = [self.getOneProduct(p)for p in self.xmlToList()]
        self.producers = set(self.producers)

    def xmlToList(self):
        return list(XmltoList(self.root))

    def getOneProduct(self, product: dict) -> dict:
        """TODO: Return one product wiht our keys
        '''/32/47/1_max.jpg'''
        """
        category_id = self.categories_mapping[int(product['kategoria_id'])]
        quantity = 0
        weight = None
        main_photo = self.set_photos(product)

        if product['liczba_egzemplarzy'] is not None:
            quantity = int(float(product['liczba_egzemplarzy']))

        if product['waga'] is not None:
            weight = float(product['waga'])

        try:
            self.producers.append(product['producent'])
        except Exception as e:
            print(product['id'], e)

        product = {
            "id": product['id'],
            "product_id": product['id'],
            "name": product['nazwa'],
            "price": float(product['cena']),
            "description": product['opis'],
            "weight": weight,
            "vat": int(product['stawka_vat']) / 100,
            "active": int(product['wyswietlenie']),
            "category_id": category_id,
            "quantity": quantity,
            "photo": main_photo
        }

        return product

    def set_photos(self, product: list) -> str:
        main_photo = None
        product_id = product['id']

        if product['zdjecia'] is not None:
            photos_id = product['zdjecia'].split(',')
            for photo_id in photos_id:
                photo = f"{product_id}_{photo_id}.jpg"

                if main_photo is None:
                    main_photo = photo

                self.images.append({
                    "product_id": product['id'],
                    "photo": photo
                })

        return main_photo
