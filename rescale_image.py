# import os
from io import BytesIO
import requests
from PIL import Image
from config_parser import settings

"""
File: rescale_image.py
Author: Rafal Marguzewicz
Email: rafal.marguzewicz@incepton.com
Github: https://github.com/incepton/
Description:
    1. list of all urls
    2. set list to agent queue
    3. ex. 5-10 worker
    4. every employee has 1-10 threads
"""

path_image = settings['folder_image']
path_thumbnails = settings['folder_thumbnails']
watermark_filename = settings['watermark_filename']


def save_image(url, filename):
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    img.save(filename)


def download_image_from_url(url, filename):
    response = requests.get(url)
    try:
        img = Image.open(BytesIO(response.content))
        small_img = img.resize((250, 250), Image.BILINEAR)
        img = watermark_with_transparency(img)

        small_img.save(f'{path_image}/250x250___{filename}')
        img.save(f'{path_image}/1024x600___{filename}')
        return True
    except Exception as e:
        print(url, filename, e)

        return False


def watermark_with_transparency(
        img: Image,
        watermark_filename: str=watermark_filename,
        position: tuple=(0, 0)) -> Image:

    watermark = Image.open(watermark_filename).convert('RGBA')

    img.paste(watermark, position, mask=watermark)
    return img


if __name__ == '__main__':
    watermark_with_transparency()
