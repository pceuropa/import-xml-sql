from config_parser import settings
from sqlalchemy import create_engine

if settings['db']['debug']:
    from logs.logs_sql import debug_mode
    debug_mode(settings['db']['logger'])

db = settings['db']

engine = create_engine(f'mysql+pymysql://{db["user"]}:{db["password"]}@{db["host"]}/{db["dbname"]}', echo=db['echo'])
connect = engine.connect()
