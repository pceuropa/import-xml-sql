from config_parser import settings
from shema import Product, ProductImage, ProductAttribute
from import_xml import Import_from_XML
from rescale_image import download_image_from_url
from datetime import datetime

import_xml = Import_from_XML()
products = import_xml.products
images = import_xml.images
attributes = import_xml.attributes
producers = import_xml.producers

"""
File: main.py
Author: Rafal Marguzewicz
Email: info@pceuropa.net
Github: https://github.com/pceuropa/
Description:
    1. import products
    2. upsert images - index unique(product_id, image)
    3. upsert attributes
    5. test - insert new products
"""


def delete_all():
    Product().truncate_table()
    ProductImage().truncate_table()
    ProductAttribute().truncate_table()


def upsert_products(data):
    n = 0
    while n < len(data):
        Product().upsert(data[n:n + 100])
        n += 100


def upsert_images(data):
    n = 0
    while n < len(images):
        ProductImage().upsert(data[n:n + 100])
        n += 100


def upsert_attributes(data):
    n = 0
    while n < len(data):
        ProductAttribute().upsert(data[n:n + 100])
        n += 100


def insert_char_to(string: str, position: int=2, char: str='/') -> str:
    return string[:position] + char + string[position:]


def download_images(images: dict={}):
    """Download all images from shop to folder download """

    for img in images:
        photo = img['photo'].split('_')[1]

        photo_download = photo.replace('.', '_max.')
        product_id = img['product_id']

        if len(product_id) > 2:
            product_id = insert_char_to(product_id)

        image_url = f"{settings['images_url']}/{product_id}/{photo_download}"
        filename = f"{img['product_id']}_{photo}"

        if download_image_from_url(image_url, filename):
            print('ok:', image_url)
        else:
            print('error:', image_url)


if __name__ == "__main__":
    """TODO: Docstring for stock.
    :returns: TODO
    """
    # delete_all()
    try:
        upsert_products(products)
    except Exception as e:
        print(e)

    new_products = Product().find_new_products()

    if new_products:
        '''Upsert Images in DB'''
        try:
            upsert_images(images)
        except Exception as e:
            pass

        id_of_new_products = set([e[0] for e in new_products])
        print("new id", id_of_new_products)
        '''Stage: update attributes'''
        attributes = Product().get_attributes(new_products)
        upsert_attributes(attributes)
        '''Download new images'''
        new_images = (img for img in images if int(img['product_id']) in id_of_new_products)
        download_images(new_images)
    else:
        print("Finish work today:", datetime.now())
