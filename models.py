# coding: utf-8
from sqlalchemy import BigInteger, Column, Date, DateTime, Float, Index, Integer, Numeric, SmallInteger, String, Table, Text, Time, text
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class Client(Base):
    __tablename__ = 'client'

    id = Column(Integer, primary_key=True)
    login = Column(String(100))
    date = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    fv = Column(Integer)
    fv_nip = Column(String(13))
    fv_company = Column(String(255))
    fv_address = Column(String(255))
    fv_postcode = Column(String(6))
    fv_city = Column(String(255))
    fv_country = Column(String(255))
    d_company = Column(String(255), nullable=False)
    d_name = Column(String(255), nullable=False)
    d_address = Column(String(255), nullable=False)
    d_postcode = Column(String(6), nullable=False)
    d_city = Column(String(255), nullable=False)
    d_country = Column(String(255), nullable=False)
    d_phone = Column(String(255), nullable=False)
    d_email = Column(String(255))
    c_name = Column(String(255), nullable=False)
    c_surname = Column(String(255))
    c_email = Column(String(255), nullable=False)
    c_phone = Column(String(255), nullable=False)
    c_postcode = Column(String(255))
    c_city = Column(String(255))
    c_address = Column(String(255))
    c_country = Column(String(255), nullable=False, server_default=text("'Polska'"))
    password = Column(String(32))
    fav = Column(Text)
    type = Column(String(30), nullable=False, server_default=text("'indywidualny'"))
    rabat = Column(Integer, nullable=False, server_default=text("'0'"))
    active = Column(Integer, server_default=text("'1'"))
    auth_key = Column(String(32))
    password_reset_token = Column(String(255))


class CmsBanner(Base):
    __tablename__ = 'cms_banner'

    id = Column(Integer, primary_key=True)
    language = Column(String(2), nullable=False, server_default=text("'pl'"))
    title = Column(String(250), nullable=False)
    content = Column(Text, nullable=False)
    link = Column(String(250), nullable=False)
    photo = Column(String(250))
    active = Column(Integer, nullable=False)
    position = Column(Integer, nullable=False, server_default=text("'1'"))


class CmsBox(Base):
    __tablename__ = 'cms_box'
    __table_args__ = (
        Index('var', 'var', 'language', unique=True),
    )

    id = Column(Integer, primary_key=True)
    var = Column(String(80), nullable=False)
    language = Column(String(2), nullable=False, server_default=text("'pl'"))
    title = Column(String(255), nullable=False)
    content = Column(Text, nullable=False)
    link = Column(String(250))
    meta_title = Column(String(250))
    meta_desc = Column(String(250))


class CmsI18n(Base):
    __tablename__ = 'cms_i18n'
    __table_args__ = (
        Index('var', 'var', 'language', unique=True),
    )

    id = Column(Integer, primary_key=True)
    var = Column(String(20), nullable=False)
    language = Column(String(2), nullable=False, server_default=text("'pl'"))
    value = Column(String(512), nullable=False)


class CmsNewsletter(Base):
    __tablename__ = 'cms_newsletter'

    id = Column(Integer, primary_key=True)
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    title = Column(String(255), nullable=False)
    content = Column(Text, nullable=False)
    include_jpg = Column(Integer, nullable=False, server_default=text("'0'"))
    sfrom = Column(Time, nullable=False)
    sto = Column(Time, nullable=False)


class CmsNewsletterEmail(Base):
    __tablename__ = 'cms_newsletter_email'

    id = Column(Integer, primary_key=True)
    group_id = Column(Integer, nullable=False, server_default=text("'1'"))
    mail = Column(String(255), nullable=False)
    active = Column(Integer, nullable=False, server_default=text("'0'"))


class CmsNewsletterGroup(Base):
    __tablename__ = 'cms_newsletter_group'

    id = Column(Integer, primary_key=True)
    title = Column(String(255), nullable=False)


class CmsNewsletterHistory(Base):
    __tablename__ = 'cms_newsletter_history'

    id = Column(Integer, primary_key=True)
    title = Column(String(255), nullable=False)
    date_from = Column(DateTime, nullable=False)
    date_to = Column(DateTime)


class CmsNewsletterSend(Base):
    __tablename__ = 'cms_newsletter_send'

    id = Column(Integer, primary_key=True)
    newsletter_id = Column(Integer, nullable=False)
    email_id = Column(Integer, nullable=False)
    send = Column(DateTime)
    history_id = Column(Integer, nullable=False)


class CmsPage(Base):
    __tablename__ = 'cms_page'

    id = Column(Integer, primary_key=True)
    language = Column(String(2), nullable=False, server_default=text("'pl'"))
    parent_id = Column(Integer, server_default=text("'0'"))
    position = Column(Integer, nullable=False, server_default=text("'0'"))
    title = Column(String(255), nullable=False)
    content = Column(Text, nullable=False)
    type_id = Column(String(50), nullable=False)
    link = Column(String(250), nullable=False)
    template = Column(String(64, 'utf8_unicode_ci'), nullable=False, server_default=text("'view'"))
    meta_title = Column(String(250), nullable=False)
    meta_desc = Column(String(250), nullable=False)
    active = Column(Integer, nullable=False, server_default=text("'1'"))
    sort_tree = Column(String(50))


class Delivery(Base):
    __tablename__ = 'delivery'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    price = Column(Numeric(10, 2), nullable=False)
    pobranie = Column(Integer, nullable=False, server_default=text("'0'"))
    free = Column(Numeric(10, 2), nullable=False)
    active = Column(Integer, nullable=False, server_default=text("'1'"))
    info = Column(String(255), nullable=False)
    payment_method = Column(String(255), nullable=False)


class Image(Base):
    __tablename__ = 'image'

    id = Column(Integer, primary_key=True)
    filePath = Column(String(400, 'utf8_unicode_ci'), nullable=False)
    itemId = Column(Integer, nullable=False)
    isMain = Column(Integer)
    modelName = Column(String(150, 'utf8_unicode_ci'), nullable=False)
    urlAlias = Column(String(400, 'utf8_unicode_ci'), nullable=False)
    name = Column(String(80, 'utf8_unicode_ci'))


class Migration(Base):
    __tablename__ = 'migration'

    version = Column(String(180), primary_key=True)
    apply_time = Column(Integer)


class Order(Base):
    __tablename__ = 'order'

    id = Column(Integer, primary_key=True)
    client_id = Column(Integer)
    status = Column(Integer, nullable=False, server_default=text("'0'"))
    products_price = Column(Numeric(10, 2), nullable=False)
    transport_price = Column(Numeric(10, 2), nullable=False)
    transport_name = Column(String(255), nullable=False)
    transport_number = Column(String(255))
    payment_price = Column(Numeric(10, 2), nullable=False, server_default=text("'0.00'"))
    payment_name = Column(String(255), nullable=False)
    payment_number = Column(String(255))
    notice = Column(String(255), nullable=False)
    date = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    fv = Column(Integer, server_default=text("'0'"))
    fv_nip = Column(String(13))
    fv_company = Column(String(255))
    fv_address = Column(String(255))
    fv_postcode = Column(String(6))
    fv_city = Column(String(255))
    fv_country = Column(String(255))
    d_name = Column(String(255), nullable=False)
    d_company = Column(String(255), nullable=False)
    d_address = Column(String(255), nullable=False)
    d_postcode = Column(String(6), nullable=False)
    d_city = Column(String(255), nullable=False)
    d_country = Column(String(255), nullable=False)
    d_phone = Column(String(255), nullable=False)
    c_name = Column(String(255), nullable=False)
    c_surname = Column(String(255))
    c_email = Column(String(255), nullable=False)
    c_phone = Column(String(255), nullable=False)
    rabat_name = Column(String(255))
    rabat = Column(Numeric(10, 2), nullable=False, server_default=text("'0.00'"))
    voucher_price = Column(Numeric(10, 2), nullable=False, server_default=text("'0.00'"))
    voucher_percent = Column(Numeric(5, 2), nullable=False, server_default=text("'0.00'"))
    voucher_name = Column(String(255))


class OrderItem(Base):
    __tablename__ = 'order_item'

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, nullable=False)
    product_id = Column(Integer, nullable=False)
    name = Column(String(255), nullable=False)
    price_one = Column(Numeric(10, 2), nullable=False)
    quantity = Column(Integer, nullable=False)
    stocks = Column(Integer)
    options = Column(Text)


class Setting(Base):
    __tablename__ = 'setting'

    id = Column(Integer, primary_key=True)
    name = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    value = Column(String(255, 'utf8_unicode_ci'))
    description = Column(Text(collation='utf8_unicode_ci'))
    field_type = Column(String(255, 'utf8_unicode_ci'))
    category = Column(String(255, 'utf8_unicode_ci'))


class ShopAttribute(Base):
    __tablename__ = 'shop_attribute'

    id = Column(Integer, primary_key=True)
    name = Column(String(255, 'utf8_polish_ci'), nullable=False)
    position = Column(Integer, nullable=False, index=True, server_default=text("'9999'"))
    show_category = Column(Integer, index=True)


class ShopCategory(Base):
    __tablename__ = 'shop_category'

    id = Column(Integer, primary_key=True)
    import_id = Column(Integer, nullable=False)
    parent = Column(Integer, nullable=False)
    position = Column(Integer, server_default=text("'0'"))
    title = Column(String(255), nullable=False)
    active = Column(Integer, nullable=False, server_default=text("'1'"))
    margin = Column(Float(5), nullable=False, server_default=text("'0.00'"))
    filters = Column(Text)
    description = Column(Text)
    slug = Column(String(255))


t_shop_category_attribute = Table(
    'shop_category_attribute', metadata,
    Column('category_id', Integer, nullable=False),
    Column('attribute_id', Integer, nullable=False),
    Column('value', String(100, 'utf8_polish_ci'), nullable=False),
    Column('unit', String(10, 'utf8_polish_ci'), nullable=False)
)


class ShopConfigurator(Base):
    __tablename__ = 'shop_configurator'

    id = Column(Integer, primary_key=True)
    type = Column(String(20), nullable=False)
    value = Column(String(100), nullable=False)
    price = Column(Float, nullable=False)
    product_id = Column(Integer)


class ShopGuarantee(Base):
    __tablename__ = 'shop_guarantee'

    id = Column(Integer, primary_key=True)
    guarantee_id = Column(String(5, 'utf8_polish_ci'), nullable=False)
    name = Column(String(255, 'utf8_polish_ci'), nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    updated_at = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))


class ShopProducer(Base):
    __tablename__ = 'shop_producer'

    id = Column(Integer, primary_key=True)
    producer_id = Column(String(50), unique=True)
    name = Column(String(100), nullable=False)


class ShopProduct(Base):
    __tablename__ = 'shop_product'

    id = Column(BigInteger, primary_key=True)
    product_id = Column(Integer, nullable=False, index=True)
    part_number = Column(String(200, 'utf8_polish_ci'))
    category_id = Column(Integer, index=True)
    import_category_id = Column(Integer)
    name = Column(String(255, 'utf8_polish_ci'), nullable=False, index=True)
    description = Column(String(collation='utf8_polish_ci'))
    price_import = Column(Float(10))
    price_basic = Column(Float(10), server_default=text("'0.00'"))
    price = Column(Float(10), nullable=False, index=True)
    promotion = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    promotion_time = Column(DateTime)
    bargain = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    recommended = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    bestseller = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    new = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    quantity = Column(SmallInteger, nullable=False, server_default=text("'0'"))
    producer_id = Column(SmallInteger, index=True)
    warranty = Column(String(4, 'utf8_polish_ci'))
    type = Column(Integer, server_default=text("'1'"))
    czas_dostawy = Column(Integer, server_default=text("'1'"))
    vat = Column(Float(2))
    vat_percent = Column(Integer)
    photo = Column(String(100, 'utf8_polish_ci'))
    active = Column(Integer, nullable=False, index=True, server_default=text("'1'"))
    open = Column(Integer, server_default=text("'0'"))
    mark = Column(Integer)
    konfigurator = Column(String(collation='utf8_polish_ci'))
    widget_offer = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    widget_recommend = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    widget_price = Column(Integer, nullable=False, server_default=text("'0'"))
    update_done = Column(Integer, server_default=text("'0'"))
    update_new = Column(Integer, server_default=text("'0'"))
    import_photo = Column(DateTime)
    weight = Column(Numeric(10, 2))
    import_done = Column(Integer, server_default=text("'0'"))
    created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    updated = Column(DateTime)


class ShopProductAttribute(Base):
    __tablename__ = 'shop_product_attribute'

    id = Column(Integer, primary_key=True)
    category_id = Column(Integer, nullable=False, index=True)
    product_id = Column(BigInteger, nullable=False, index=True)
    attribute_id = Column(SmallInteger, nullable=False, index=True)
    value = Column(String(100, 'utf8_polish_ci'), nullable=False, index=True)
    unit = Column(String(10, 'utf8_polish_ci'))


class ShopProductImage(Base):
    __tablename__ = 'shop_product_image'
    __table_args__ = (
        Index('unique_index', 'product_id', 'photo', unique=True),
    )

    id = Column(Integer, primary_key=True)
    product_id = Column(BigInteger, nullable=False, index=True)
    photo = Column(String(80), nullable=False)
    position = Column(Integer)


class ShopStatu(Base):
    __tablename__ = 'shop_status'

    id = Column(Integer, primary_key=True)
    status_id = Column(Integer, nullable=False)
    template = Column(Text, nullable=False)
    mail = Column(Integer, nullable=False, server_default=text("'0'"))
    sms = Column(Integer, nullable=False, server_default=text("'0'"))


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String(255, 'utf8_unicode_ci'), nullable=False, unique=True)
    auth_key = Column(String(32, 'utf8_unicode_ci'), nullable=False)
    password_hash = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    password_reset_token = Column(String(255, 'utf8_unicode_ci'), unique=True)
    email = Column(String(255, 'utf8_unicode_ci'), nullable=False, unique=True)
    status = Column(SmallInteger, nullable=False, server_default=text("'10'"))
    created_at = Column(Integer, nullable=False)
    updated_at = Column(Integer, nullable=False)
    group_id = Column(Integer, nullable=False)
    access_level = Column(Integer, nullable=False, server_default=text("'0'"))
    is_deleted = Column(Integer, nullable=False, server_default=text("'0'"))
    last_login = Column(Integer)
    first_name = Column(String(50, 'utf8_unicode_ci'))
    last_name = Column(String(50, 'utf8_unicode_ci'))


class Voucher(Base):
    __tablename__ = 'voucher'

    id = Column(Integer, primary_key=True)
    price = Column(Numeric(10, 2), server_default=text("'0.00'"))
    percent = Column(Integer, server_default=text("'0'"))
    code = Column(String(255), nullable=False)
    desc = Column(String(255), nullable=False)
    quantity = Column(Integer, nullable=False)
    date_from = Column(Date, nullable=False)
    date_to = Column(Date, nullable=False)
    client = Column(Integer, nullable=False, server_default=text("'0'"))
    mail = Column(String(120))


class VoucherCode(Base):
    __tablename__ = 'voucher_codes'

    id = Column(Integer, primary_key=True)
    voucher_id = Column(Integer, nullable=False)
    code = Column(String(11), nullable=False)
    order_id = Column(Integer)
