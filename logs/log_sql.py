from time import time
from sqlalchemy import event
from sqlalchemy.engine import Engine
from logs.log import setup_logger


def debug_mode(settings):
    lo = setup_logger(settings)

    @event.listens_for(Engine, "before_cursor_execute")
    def before_cursor_execute(conn, cursor, statement, parameters, context, executemany):
        conn.info.setdefault('query_start_time', []).append(time())

    @event.listens_for(Engine, "after_cursor_execute")
    def after_cursor_execute(conn, cursor, statement, parameters, context, executemany):
        t = time() - conn.info['query_start_time'].pop(-1)
        if t > 0.002:
            lo.debug(f"{t:f} {statement} {parameters}")
