import os
from pathlib import Path
from json import load
import yaml


def config(filename='settings.yml'):
    try:
        cwd = Path.cwd()
        path_str = "{}/{}".format(cwd, filename)
        return load(open(path_str))
    except Exception as e:
        print(e)


def config_yaml(filename='settings.yml'):
    try:
        path = os.path.dirname(__file__)
        path_file = f"{path}/{filename}"
        file = open(path_file, 'r')
        file = open(path_file, 'r')
        settings = yaml.load(file)
        file.close()

        return settings
    except Exception as e:
        raise e


settings = config_yaml()

if os.environ.get('TESTMODE'):
    settings = settings['test']
