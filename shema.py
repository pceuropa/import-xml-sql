import logging
from db import engine, settings
# from sqlalchemy import insert, update, Table, MetaData, Column, text, ForeignKey, UniqueConstraint
from sqlalchemy import insert, update, Table, MetaData, Column, text
from sqlalchemy import Integer, String, SmallInteger, Numeric, DateTime
from sqlalchemy.dialects.mysql import insert as insertMysql
from datetime import datetime

metadata = MetaData(bind=engine)
connect = engine.connect()

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.FATAL)


class Main(object):
    conn = engine.connect()

    def create_table(self):
        self.table.create(engine, checkfirst=True)

    def drop_table(self):
        self.table.drop(engine, checkfirst=True)

    def truncate_table(self):
        self.table.delete().execute()

    def all(self):
        sql = text(f"select * from {self.table_name}")
        return connect.execute(sql).fetchall()

    def insert(self, data):
        try:
            insert(self.table).values(data).execute()
        except Exception as e:
            raise e

    def update(self, data={}):
        try:
            update(self.table).values(**data).execute()
        except Exception as e:
            raise e


class Product(Main):
    table_name = 'shop_product'
    table = Table(table_name, metadata,
            Column('id', Integer, primary_key=True),
            Column('product_id', Integer, nullable=False),
            Column('part_number', String(15), nullable=False),
            Column('category_id', Integer, nullable=False),
            Column('name', String(255), nullable=False),
            Column('description', String()),
            Column('price', Numeric()),
            Column('weight', Numeric()),
            Column('producer_id', SmallInteger()),
            Column('vat', Numeric()),
            Column('photo', String(100), nullable=False),
            Column('active', SmallInteger),
            Column('category_id', SmallInteger),
            Column('quantity', SmallInteger),
            Column('created', DateTime, server_default=text("current_timestamp")),
            Column('updated', DateTime))

    def upsert(self, data):
        conn = engine.connect()
        insert_stmt = insertMysql(self.table).values(data)
        stmt = insert_stmt.on_duplicate_key_update(
            active=insert_stmt.inserted.active,
            quantity=insert_stmt.inserted.quantity,
            price=insert_stmt.inserted.price,
            vat=insert_stmt.inserted.vat,
            updated=datetime.now()
        )

        try:
            conn.execute(stmt)
            conn.close()
        except Exception as e:
            raise e

    def all(self):
        """Query all rows
        :returns: dict
        """
        sql = text(f"select id from {self.table_name};")
        return [r for r, in self.conn.execute(sql).fetchall()]

    def find_new_products(self):
        sql = text(f"select id, name, description, category_id from {self.table_name} where date(created) = curdate();")
        return self.conn.execute(sql).fetchall()

    def get_attributes(self, new_products: dict={}, cat: int=1) -> dict:
        """
        Actually set attributes for one category. But ready to many categories
        """
        attributes = []
        attributes_mapping = settings['attributes_mapping'][cat]
        for prod in new_products:

            id = prod[0]
            name = prod[1] or ''
            description = prod[2] or ''
            category_id = prod[3]

            if category_id != cat:
                continue

            for key in attributes_mapping:
                if key in name or key in description:
                    attributes.append({
                        "product_id": id,
                        "category_id": category_id,
                        "attribute_id": cat,
                        "value": attributes_mapping[key]
                    })
        return attributes


class ProductImage(Main):
    table_name = 'shop_product_image'
    table = Table(table_name, metadata,
            Column('id', Integer, primary_key=True),
            Column('product_id', Integer, nullable=False),
            Column('photo', String(255), nullable=False),
            Column('position', Numeric()))

    def upsert(self, data):
        conn = engine.connect()
        insert_stmt = insertMysql(self.table).prefix_with('IGNORE').values(data)

        try:
            conn.execute(insert_stmt)
            conn.close()
        except Exception as e:
            raise e

    def count(self, id):
        sql = text(f"select count(1) from {self.table_name};")
        r = self.connect.execute(sql).fetchone()
        return r[0]


class ProductAttribute(Main):
    table_name = 'shop_product_attribute'
    table = Table(table_name, metadata,
            Column('id', Integer, primary_key=True),
            Column('category_id', Integer),
            Column('product_id', Integer),
            Column('attribute_id', Integer),
            Column('value', String(100)),
            Column('unit', String(10)),
            Column('position', Numeric()))

    def upsert(self, data):
        conn = engine.connect()
        insert_stmt = insertMysql(self.table).values(data)

        try:
            conn.execute(insert_stmt)
            conn.close()
        except Exception as e:
            raise e


if __name__ == "__main__":
    metadata.drop_all()
    metadata.create_all()
